<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Provide form callbacks for migrate source plugins.
 */

function forgery__STUB__form($form, FormStateInterface $form_state) {
  return [];
}

/**
 * Default form builder
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @return array
 */
function forgery__default_form_builder($form, FormStateInterface $form_state) {
  $skip_keys = ['plugin', 'source'];
  if (isset($form['_process_plugin_existing_fields'])) {
    $process_plugin_fields = $form['_process_plugin_existing_fields'];
    foreach ($process_plugin_fields as $key => $value) {
      if (in_array($key, $skip_keys)) {
        continue;
      }

      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $key,
        '#value' => $value
      ];
    }

    unset($form['_process_plugin_existing_fields']);
    return $form;
  }

  return [];
}

/**
 * @see \Drupal\migrate\Plugin\migrate\process\DefaultValue
 */
function forgery__default_value__form($form, FormStateInterface $form_state): array {
  $form['default_value'] = [
    '#type' => 'textfield',
    '#title' => 'Default value',
    '#description' => t('The fixed default value to apply.')
  ];
  $form['strict'] = [
    '#type' => 'checkbox',
    '#title' => t('Use strict value checking'),
    '#description' => t('Only apply default value when input value is NULL.'),
  ];

  return $form;
}

/**
 * @see \Drupal\migrate\Plugin\migrate\process\MigrationLookup
 */
function forgery__migration_lookup__form($form, FormStateInterface $form_state): array {

  $form['migration'] = [
    '#type' => 'select',
    '#title' => 'Source migration',
    '#description' => t('A single migration ID, or an array of migration IDs.'),
    '#options' => [],
    '#cardinality' => -1,
  ];


  $migration_storage = Drupal::entityTypeManager()->getStorage('migration');
  foreach ($migration_storage->loadMultiple() as $item) {
    // TODO ADD TO MIGRATION DEPENDENCIES!
    $form['migration']['#options'][$item->id()] = $item->label();
  }


  $form['source_ids'] = [
    '#type' => 'textfield',
    '#title' => 'Source IDs',
    '#description' => t('An array keyed by migration IDs with values that are a list of source properties.'),
    '#cardinality' => -1,
  ];

  $form['stub_id'] = [
    '#type' => 'textfield',
    '#title' => 'Stub ID',
    '#description' => t('Identifies the migration which will be used to create any stub entities.'),
  ];

  $form['no_stub'] = [
    '#type' => 'checkbox',
    '#title' => 'Avoid using stub entities',
    '#description' => t('Prevents the creation of a stub entity when no relationship is found in the migration map.'),
    '#cardinality' => -1,
  ];

  return $form;
}

/**
 * @see \Drupal\migrate\Plugin\migrate\process\FormatDate
 */
function forgery__format_date__form($form, FormStateInterface $form_state): array {

  $form['from_format'] = [
    '#type' => 'textfield',
    '#title' => t('From format'),
    '#description' => t('The source format string.'),
  ];
  $form['to_format'] = [
    '#type' => 'textfield',
    '#title' => t('To format'),
    '#description' => t('The destination format.'),
  ];

  $form['from_timezone'] = [
    '#type' => 'select',
    '#title' => t('From Timezone'),
    '#description' => t('String identifying the required source time zone.'),
  ];

  $form['to_timezone'] = [
    '#type' => 'select',
    '#title' => t('To Timezone'),
    '#description' => t('String identifying the required destination time zone.'),
  ];

  $form['settings'] = [
    '#type' => 'checkboxes',
    '#title' => t('Settings'),
    '#options' => [
      'validate_format' => t('Validate format'),
    ],
  ];

  return $form;
}


/**
 * @see \Drupal\migrate\Plugin\migrate\process\StaticMap
 */
function forgery__static_map__form($form, FormStateInterface $form_state): array {

  $form['map'] = [
    '#type' => 'fieldgroup',
    '#title' => t('Map'),
    '#description' => t('An array (of 1 or more dimensions) that defines the mapping between source values and destination values'),
    '#tree' => TRUE,
  ];

  $process_plugin = $form['#process_plugin'];
  foreach ($process_plugin['map'] ?? [] as $source => $target) {
    $form['map'][] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex; gap: 1rem;',
      ],
      'key' => [
        '#type' => 'textfield',
        '#title' => t('Source value'),
        '#value' => $source,
        '#size' => 32,
      ],
      'value' => [
        '#type' => 'textfield',
        '#title' => t('Target value'),
        '#value' => $target,
        '#size' => 36,
      ],
    ];
  }
  // TODO CARDINALITY -1 AND MULTILEVEL?

  $form['bypass'] = [
    '#type' => 'checkbox',
    '#title' => 'Return the unmodified input value',
    '#description' => t('Whether the plugin should proceed when the source is not found in the map array.'),
  ];

  $form['default_value'] = [
    '#type' => 'textfield',
    '#title' => 'Default value',
    '#description' => t('The value to return if the source is not found in the map array.')
  ];

  return $form;
}

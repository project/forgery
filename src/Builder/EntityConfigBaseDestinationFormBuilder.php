<?php

namespace Drupal\forgery\Builder;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * @see \Drupal\migrate\Plugin\migrate\destination\EntityConfigBase
 */
class EntityConfigBaseDestinationFormBuilder implements DestinationFormBuilderInterface {

  use StringTranslationTrait;

  public function buildForm($form, $plugin_id): array {
    $form['translations'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Destination language code'),
    ];

    return $form;
  }

}

<?php

namespace Drupal\forgery\Builder;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate_plus\Entity\MigrationInterface;

/**
 * @see \Drupal\migrate\Plugin\migrate\destination\EntityContentBase
 */
class EntityContentBaseDestinationFormBuilder implements DestinationFormBuilderInterface {

  use StringTranslationTrait;

  public function buildForm($form, $plugin_id): array {
    [$plugin_base_id, $entity_type_id] = explode(':', $plugin_id, 2);

    $form['default_bundle'] = [
      '#type' => 'select',
      '#options' => $this->getBundleOptions($entity_type_id),
      '#title' => $this->t('Default bundle'),
    ];

    $form['validate'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Validate entity'),
    ];

    return $form;
  }

  protected function getBundleOptions($entity_type) {
    $options = [
      '' => $this->t('Unset')
    ];

    $bundle_info_service = \Drupal::service('entity_type.bundle.info');
    foreach ($bundle_info_service->getBundleInfo($entity_type) as $bundle => $bundle_info) {
      $options[$bundle] = $bundle;
    }

    return $options;
  }

}

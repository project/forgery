<?php

namespace Drupal\forgery\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\forgery\Builder\DestinationFormBuilderInterface;
use Drupal\migrate_plus\Entity\MigrationGroup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class MigrationForm extends EntityForm {

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $migration = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $migration->label(),
      '#description' => $this->t("Label for the Example."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $migration->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$migration->isNew(),
    ];

    $migration_group = $this->getRouteMatch()->getParameter('migration_group');

    $form['migration_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Migration group'),
      '#options' => [],
      '#default_value' => $migration_group instanceof MigrationGroup ? $migration_group->id() : NULL,
    ];

    $group_storage = $this->entityTypeManager->getStorage('migration_group');
    foreach ($group_storage->loadMultiple() as $item) {
      $form['migration_group']['#options'][$item->id()] = $item->label();
    }

    /** @var \Drupal\migrate\Plugin\MigrateSourcePluginManager $source_plugin_manager */
    $source_plugin_manager = \Drupal::service('plugin.manager.migrate.source');
    $definitions = $source_plugin_manager->getDefinitions();
    $options = [];
    foreach ($definitions as $name => $data) {
      $options[$name] = $name;
    }

    $form['definition'] = array(
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-source',
    );

    $form['source'] = array(
      '#type' => 'details',
      '#title' => $this->t('Source'),
      '#group' => 'information',
      '#open' => TRUE,
      '#tree' => TRUE,
    );

    $form['source']['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Source plugin'),
      '#options' => $options,
      '#default_value' => $migration->get('source')['plugin'] ?? '',
    ];


    $form['migrate_destination'] = [
      '#type' => 'details',
      '#title' => $this->t('Destination'),
      '#group' => 'information',
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    /** @var \Drupal\migrate\Plugin\MigrateDestinationPluginManager $destination_plugin_manager */
    $destination_plugin_manager = \Drupal::service('plugin.manager.migrate.destination');
    $definitions = $destination_plugin_manager->getDefinitions();
    $options = [];
    foreach ($definitions as $name => $data) {
      $options[$name] = $name;
    }

    $form['migrate_destination']['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination plugin'),
      '#options' => $options,
      '#default_value' => $migration->get('destination')['plugin'] ?? '',
      '#ajax' => [
        'callback' => '::destinationAjaxCallback',
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'wrapper' => 'edit-migrate-destination-config',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading configuration...'),
        ],
      ],
    ];

    $form['migrate_destination']['config'] = [
      '#type' => 'container',
    ];

    if (isset($migration->get('destination')['plugin']) && $migration->get('destination')['plugin']) {
      $form['migrate_destination']['config'] += static::destinationConfigForm($migration->get('destination')['plugin']);

      foreach ($migration->get('destination') ?? [] as $config => $value) {
        $form['migrate_destination']['config'][$config]['#value'] = $value;
      }
    }

    $form['migration_dependencies'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Migration dependencies'),
    ];

    $dependencies = $migration->get('migration_dependencies');
    $required_string = '';
    $optional_string = '';
    if (isset($dependencies['required']) && is_array($dependencies['required']) && count($dependencies['required']) > 0) {
      // TODO LOAD ENTITIES
      $required_string = '(' . implode('), (', $dependencies['required']).')';
    }

    if (isset($dependencies['optional']) && is_array($dependencies['optional']) && count($dependencies['optional']) > 0) {
      // TODO LOAD ENTITIES
      $optional_string = '(' . implode('), (', $dependencies['optional']).')';
    }

    $form['migration_dependencies']['required'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'migration',
      '#selection_handler' => 'default',
      '#title' => $this->t('Required'),
      '#value' => $required_string,
    ];

    $form['migration_dependencies']['optional'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'migration',
      '#tags' => TRUE,
      '#selection_handler' => 'default',
      '#title' => $this->t('Optional'),
      '#value' => $optional_string,
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $migration = $this->entity;

    if ($migration->isNew()) {
      $migration->set('dependencies', [
        'enforced' => [
          'module' => [
            'forgery'
          ],
        ],
      ]);

      $migration->set('process', []);
    }

    $migrate_destination = $migration->get('migrate_destination');
    $migrate_destination_config = array_filter($migrate_destination['config'] ?? []);
    unset($migrate_destination['config']);
    $migrate_destination = $migrate_destination + $migrate_destination_config;
    $migration->set('destination', $migrate_destination);
    $migration->set('migrate_destination', NULL);

    // Fix autocomplete machine name error.
    $user_input = $form_state->getUserInput();
    $dependencies = $migration->get('migration_dependencies');

    foreach ($user_input['migration_dependencies'] as $key => $migration_dependency) {
      if (empty($migration_dependency)) {
        $dependencies[$key] = [];
        continue;
      }

      preg_match_all("/\((.*?)\)/", $migration_dependency, $matches);
      $dependencies[$key] = $matches[1];
    }

    if (empty($dependencies['required'])) {
      unset($dependencies['required']);
    }
    if (empty($dependencies['optional'])) {
      unset($dependencies['optional']);
    }

    $migration->set('migration_dependencies', $dependencies ?? []);

    $migration_tags = $migration->get('migration_tags');
    if (is_string($migration_tags)) {
      $migration->set('migration_tags', explode(',', $migration_tags));
    }
    if (empty($migration_tags)) {
      $migration->set('migration_tags', []);
    }

    $status = $migration->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Example created.', [
        '%label' => $migration->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Example updated.', [
        '%label' => $migration->label(),
      ]));
    }

    $form_state->setRedirect('entity.migration.process_form', [
      'migration' => $migration->id(),
      'migration_group' => $migration->get('migration_group')
    ]);
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('migration')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

  protected static function destinationConfigForm($plugin_id) {

    /** @var \Drupal\migrate\Plugin\MigrateDestinationPluginManager $destination_plugin_manager */
    $destination_plugin_manager = \Drupal::service('plugin.manager.migrate.destination');
    $plugin_definition = $destination_plugin_manager->getDefinition($plugin_id);

    if (isset($plugin_definition['form'])
      && is_subclass_of($plugin_definition['form'], DestinationFormBuilderInterface::class)
    ) {
      $callback = [new $plugin_definition['form'], 'buildForm'];
      return call_user_func_array($callback, [[], $plugin_id]);
    }

    return ['#markup' => 'No available configuration found'];
  }

  public static function destinationAjaxCallback(array &$form, FormStateInterface $form_state) {
    $selected_plugin_id = $form_state->getValue('migrate_destination')['plugin'];

    $form['migrate_destination']['config'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'edit-migrate-destination-config',
        'data-drupal-selector' => 'edit-migrate-destination-config',
      ]
    ] + static::destinationConfigForm($selected_plugin_id);

    return $form['migrate_destination']['config'];
  }

}

<?php

namespace Drupal\forgery\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\forgery\Resolver\MigrateDestinationFieldResolver;
use Drupal\forgery\Resolver\MigrateSourceFieldResolver;
use Drupal\migrate_plus\Entity\MigrationGroup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class MigrationProcessForm extends EntityForm {

  protected $destinationFieldResolver;
  protected $sourceFieldResolver;

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MigrateDestinationFieldResolver $destination_field_resolver,
    MigrateSourceFieldResolver $source_field_resolver
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->destinationFieldResolver = $destination_field_resolver;
    $this->sourceFieldResolver = $source_field_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('forgery.migration.destination_field_resolver'),
      $container->get('forgery.migration.source_field_resolver')
    );
  }

  public function titleCallback() {
    $migration = $this->entity ?? $this->getRouteMatch()->getParameter('migration');
    return $this->t('Edit %migration process', [
      '%migration' => $migration->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\migrate_plus\Entity\MigrationInterface $migration */
    $migration = $this->entity;

    // Get fields from the destination plugin and merge them with actual keys
    // from the process defined in the migration entity allowing users define
    // process keys that are not defined previously. (ex. pathauto)
    $destination_fields = $this->destinationFieldResolver->resolve($migration);

    // @todo logic already coded below. When source fields not available, try
    // reading from different sources, for example source definition in the
    // yaml file source. Users should be able to add source fields.
    $source_fields = $this->sourceFieldResolver->resolve($migration);

    $source_options = [
      '' => $this->t('- Empty -')
    ] + $source_fields;

    $rows = [];

    $process = $migration->get('process');
    foreach ($destination_fields as $key) {
      $source_default_value = $process[$key] ?? '';
      if (empty($source_default_value)) {
        $source_default_value = '';
      }

      $modifiers = '—';
      if (!is_string($source_default_value)) {
        $modifiers = isset($process[$key]['plugin']) ? $this->t('Plugin: %plugin', ['%plugin' => $process[$key]['plugin']]) :
          $this->t('@count plugins', ['@count' => count($process[$key])]);
      }

      if (is_string($source_default_value) && strlen($source_default_value) > 0) {
        $modifiers = 'get';
      }

      $rows[$key]['target'] = [
        'data' => [
          '#type' => 'html_tag',
          '#tag' => 'code',
          '#value' => $key,
          '#attributes' => [
            'style' => 'background-color: var(--color-red-050); padding: 2px 4px; border-radius: 3px;'
          ]
        ]
      ];

      $rows[$key]['source'] = [
        'data' => [
          '#type' => 'select',
          '#options' => $source_options,
          '#default_value' => $source_default_value,
          '#value' => $source_default_value,
          '#attributes' => [
            'name' => sprintf('mapping[%s]', $key),
          ]
        ],
      ];

      $rows[$key]['modifiers'] = $modifiers;

      $rows[$key]['operations'] = [
        'data' => [
          '#type' => 'dropbutton',
          '#dropbutton_type' => 'small',
          '#links' => [
            'simple_form' => [
              'title' => $this->t('Edit'),
              'url' => Url::fromRoute('entity.migration.modifiers_form', [
                'migration_group' => $migration->get('migration_group'),
                'migration' => $migration->id(),
              ], ['query' => ['process' => $key]]),
            ],
            'demo' => [
              'title' => $this->t('Remove'),
              'url' => Url::fromRoute('<front>'),
            ],
          ],
        ],
      ];
    }

    $header = [
      'target' => t('Target'),
      'source' => t('Source'),
      'modifiers' => t('Modifiers'),
      'operations' => t('Operations'),
    ];
    $form['table'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
      '#attached' => [
        'library' => [
          'forgery/table_styling',
        ],
      ],
      '#attributes' => [
        'class' => [
          'forgery-process-table',
        ],
      ],
    ];

    $form['config'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
      '#title' => $this->t('Migrate process settings')
    ];
    $form['config']['remove_empty_keys'] = [
      '#type' => 'checkbox',
      '#title' => 'Remove empty keys',
      '#default_value' => FALSE,
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $migration = $this->entity;

    $mapping = $form_state->getUserInput()['mapping'];

    $process_config = $form_state->getValue('config');
    $clean_mapping = $mapping;
    if ($process_config['remove_empty_keys']) {
      $clean_mapping = array_filter($mapping);
    }

    $process = $migration->get('process');

    foreach ($clean_mapping as $destination => $source) {

      if (empty($source) && empty($process[$destination])) {
        $process[$destination] = [];
        continue;
      }

      if (isset($process[$destination]) && is_string($process[$destination])) {
        $process[$destination] = $source;
        continue;
      }

      if (isset($process[$destination]['source'])) {
        $process[$destination]['source'] = $source;
        continue;
      }

      if (isset($process[$destination][0]['source'])) {
        $process[$destination][0]['source'] = $source;
        continue;
      }

      $process[$destination] = $source;
    }

    $migration->set('process', $process);

    $status = $migration->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Example created.', [
        '%label' => $migration->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Example updated.', [
        '%label' => $migration->label(),
      ]));
    }

    $form_state->setRedirect('entity.migration.process_form', [
      'migration' => $migration->id(),
      'migration_group' => $migration->get('migration_group')
    ]);
  }

}

<?php

namespace Drupal\forgery\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\forgery\Resolver\MigrateSourceFieldResolver;
use Drupal\migrate\Plugin\MigratePluginManager;
use Drupal\migrate_plus\Entity\MigrationGroup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class ProcessModifiersForm extends EntityForm {

  /**
   * Migrate process plugin manager instance.
   *
   * @var \Drupal\migrate\Plugin\MigratePluginManager
   */
  protected $processPluginManager;

  protected $sourceFieldResolver;

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    MigratePluginManager $migrate_process_plugin_manager,
    MigrateSourceFieldResolver $source_field_resolver
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->processPluginManager = $migrate_process_plugin_manager;
    $this->sourceFieldResolver = $source_field_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migrate.process'),
      $container->get('forgery.migration.source_field_resolver')
    );
  }

  public function titleCallback() {
    return $this->t('Edit %name process modifiers', [
      '%name' => $this->getRequest()->get('process'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $migration = $this->entity;

    $process = $migration->get('process');
    $process_element = $this->getRequest()->get('process');

    if (!in_array($process_element, array_keys($process))) {
      // Safety chcek for unprocessed user input.
      return ['#markup' => 'Process destination entry not found!'];
    }

    $form['process_destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination'),
      '#value' => $process_element,
      '#disabled' => TRUE,
    ];

    $source_fields = $this->sourceFieldResolver->resolve($migration);
    $form['process_plugin_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Process plugin source'),
      '#options' => ['' => $this->t('– None –')] + $source_fields,
    ];

    if (is_string($process[$process_element])) {
      $form['process'] = [
        '#markup' => $this->t('Currently the simple get plugin is used.')
      ];
      $form['process_plugin_source']['#value'] = $process[$process_element];
    }

    if (empty($process[$process_element])) {
      var_dump('empty');
    }

    $process_plugins = $process[$process_element];

    if (isset($process[$process_element]['plugin'])) {
      $process_plugins = [$process[$process_element]];
    }

    $slected_source = $process_plugins[0]['source'] ?? '';
    $form['process_plugin_source']['#value'] = $slected_source;

    $form['plugins'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    foreach ($process_plugins as $delta => $process_plugin) {
      $form['plugins'][$delta] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Plugin: %plugin', ['%plugin' => $process_plugin['plugin']]),
        '#tree' => TRUE,
      ];

      // TODO REPLACE WITH PLUGIN MANAGER
      $plugin_form_callback = 'forgery__' . $process_plugin['plugin'] . '__form';

      if (!function_exists($plugin_form_callback) && isset($process_plugin['plugin'])) {
        $plugin_form_callback = 'forgery__default_form_builder';
        $form['plugins'][$delta]['_process_plugin_existing_fields'] = $process_plugin;
      }

      if (function_exists($plugin_form_callback)) {
        $form['plugins'][$delta]['#process_plugin'] = $process_plugin;
        $config_fields = call_user_func($plugin_form_callback, $form['plugins'][$delta], $form_state);

        foreach ($process_plugin as $key => $value) {
          if (isset($config_fields[$key]) && empty($config_fields[$key]['#value'])) {
            $config_fields[$key]['#value'] = $value;
          }
        }

        $form['plugins'][$delta] = [
          'plugin' => [
            '#type' => 'hidden',
            '#value' => $process_plugin['plugin'],
          ],
        ] + ($config_fields ?? []);
      }
    }

    $definitions = $this->processPluginManager->getDefinitions();
    foreach ($definitions as $plugin_id => $definition) {
      $options[$plugin_id] = $plugin_id;
    }

    $form['add'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add new process plugin'),
      '#tree' => TRUE,
      '#attached' => [
        'library' => [
          'forgery/table_styling',
        ],
      ],
    ];

    $form['add']['plugin'] = [
      '#type' => 'select',
      '#options' => ['' => $this->t('– Select a plugin to add –')] + $options,
      '#title' => $this->t('Select process plugin'),
    ];


    $form['process_element'] = [
      '#type' => 'hidden',
      '#value' => $process_element,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $migration = $this->entity;

    $new_process_plugin = $form_state->getValue('add');
    $process_element = $form_state->getValue('process_element');

    $process = $migration->get('process');
    $plugins = $form_state->getValue('plugins');

    $process_plugins = $process[$process_element];
    if (isset($process_plugins['plugin'])) {
      $process_plugins = [$process_plugins];
    }

    foreach ($plugins ?? [] as $delta => $plugin) {
      $process_plugins[$delta] = $plugin;
    }

    $process[$process_element] = $process_plugins;
    if (!empty($new_process_plugin) && isset($new_process_plugin['plugin'])) {
      $process_element = $form_state->getValue('process_element');

      if (is_string($process[$process_element])) {
        $process[$process_element] = $new_process_plugin + ['source' => $process[$process_element]];
      }

      if (empty($process[$process_element])) {
        $process[$process_element] = $new_process_plugin;
      }

      $process[$process_element][] = $new_process_plugin;

    }

    if (is_array($process_plugins) && count($process_plugins) === 1) {
      $process_plugins = reset($process_plugins);
    }

    $migration->set('process', $process);

    $status = $migration->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Example created.', [
        '%label' => $migration->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Example updated.', [
        '%label' => $migration->label(),
      ]));
    }
  }

}

<?php

namespace Drupal\forgery\Resolver;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Link;
use Drupal\field\Entity\FieldConfig;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrateDestinationInterface;
use Drupal\migrate\Plugin\MigrateDestinationPluginManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_plus\Entity\MigrationInterface;

/**
 * Migration destination fields resolver
 */
class MigrateDestinationFieldResolver {

  /**
   * Entity Type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field manager instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  protected $migrationPluginManager;
  protected $destinationPluginManager;

  /**
   * Constructs a new MigrateDestinationFieldResolver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type manager instance.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field manager instance.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    MigrationPluginManagerInterface $migration_plugin_manager,
    MigrateDestinationPluginManager $destination_plugin_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->destinationPluginManager = $destination_plugin_manager;
  }


  public function resolve(MigrationInterface $migration): array {
    $configuration = $migration->get('destination');
    $plugin = $this->getPluginInstance($configuration['plugin'], $migration->id());

    $fields = $plugin->fields();

    if (empty($fields) && $plugin instanceof EntityContentBase) {
      $bundle = NULL;
      if (isset($configuration['default_bundle'])) {
        $bundle = $configuration['default_bundle'];
      }

      $entity_type_id = $plugin->getDerivativeId();

      $fields = $this->resolveContentEntityFields($entity_type_id, $bundle);
    }

    $process = $migration->get('process');

    foreach ($process as $field => $plugins) {
      if (!in_array($field, $fields)) {
        $fields[] = $field;
      }
    }

    return $fields;
  }

  protected function getPluginInstance($plugin_id, $migration_id): ?MigrateDestinationInterface {
    $migration_plugin = $this->migrationPluginManager->createInstance($migration_id);
    $destination_plugin_instance = $this->destinationPluginManager->createInstance(
      $plugin_id,
      [],
      $migration_plugin
    );

    return $destination_plugin_instance;
  }

  protected function resolveContentEntityFields($entity_type_id, $bundle) {
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
    $values = [];

    $fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);

    if ($bundle) {
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
    }

    foreach ($fields as $field_name => $field) {
      $cartinality = NULL;
      $columns = [];

      // TODO: RESOLVE BUNDLE KEYS??!

      if ($field_name === 'type') {
        // Skip bundle fields.
        continue;
      }

      if ($field instanceof FieldStorageDefinitionInterface) {
        $columns = $field->getColumns();
        $cartinality = $field->getCardinality();
      }
      if ($field instanceof FieldConfig) {
        $columns = $field->getFieldStorageDefinition()->getColumns();
        $cartinality = $field->getFieldStorageDefinition()->getCardinality();
      }

      if ($cartinality === 1 || BaseFieldDefinition::CARDINALITY_UNLIMITED) {
        $values[] = $field_name;
      }

      if (count($columns) === 1 && isset($columns['value'])) {
        continue;
      }

      foreach ($columns as $key => $item) {
        $values[] = sprintf('%s/%s', $field_name, $key);
      }
    }

    return $values;
  }


}

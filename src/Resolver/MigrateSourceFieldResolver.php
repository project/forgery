<?php

namespace Drupal\forgery\Resolver;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Link;
use Drupal\field\Entity\FieldConfig;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrateDestinationInterface;
use Drupal\migrate\Plugin\MigrateDestinationPluginManager;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrateSourcePluginManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_plus\Entity\MigrationInterface;

/**
 * Migration source fields resolver
 */
class MigrateSourceFieldResolver {

  /**
   * Entity Type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field manager instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  protected $migrationPluginManager;

  protected $sourcePluginManager;

  /**
   * Constructs a new MigrateDestinationFieldResolver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type manager instance.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field manager instance.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    MigrationPluginManagerInterface $migration_plugin_manager,
    MigrateSourcePluginManager $source_plugin_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->sourcePluginManager = $source_plugin_manager;
  }


  public function resolve(MigrationInterface $migration): array {
    $configuration = $migration->get('source');
    $plugin = $this->getPluginInstance($configuration['plugin'], $migration->id());
    return $plugin->fields();
  }

  protected function getPluginInstance($plugin_id, $migration_id): ?MigrateSourceInterface {
    $migration_plugin = $this->migrationPluginManager->createInstance($migration_id);
    $source_plugin_instance = $this->sourcePluginManager->createInstance(
      $plugin_id,
      [],
      $migration_plugin
    );

    return $source_plugin_instance;
  }


}

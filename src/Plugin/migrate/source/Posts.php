<?php

namespace Drupal\forgery\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Example posts migrations.
 *
 * @MigrateSource(
 *   id = "posts",
 *   source_module = "forgery",
 * )
 */
class Posts extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('Id'),
      'date' => $this->t('Date'),
      'title' => $this->t('Title'),
      'content' => $this->t('Content'),
      'image' => $this->t('Image'),
      'author' => $this->t('Author'),
      'about' => $this->t('About'),
      'uri' => $this->t('Uri'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('blog', 'T')
      ->fields('T', array_keys($this->fields()));
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
        'alias' => 'T',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    $date = $row->getSourceProperty('date');
    $datetime = date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date));
    $row->setSourceProperty('datetime', $datetime);

    $row->setSourceProperty('path', '/blog/'. $row->getSourceProperty('uri'));

    return parent::prepareRow($row);
  }
}
